<?php

declare(strict_types=1);

namespace App\Services\Propagation;

use Zipkin\Propagation\Getter;

/**
 *
 * This class implements the Zipkin\Propagation\Getter interface to extract the zipkin
 * headers out of the $_SERVER variable.
 * Due to the Getter::get signature, $_SERVER gets passed in as $carrier rather
 * than get accessed here directly.
 *
 * Example:
 *   $extractor = $this->tracing->getPropagation()->getExtractor(new ServerHeaders);
 *   $extractedContext = $extractor($_SERVER);
 */
final class CustomHeader implements Getter
{
    /**
     * {@inheritdoc}
     *
     * @param mixed $carrier
     * @param string $key
     * @return string|null
     */
    public function get($carrier, string $key): ?string
    {
        // Headers in $_SERVER are always uppercased, with any - replaced with an _
        if(!empty($carrier[$key])) {
            return (string)$carrier[$key];
        }

        return null;
    }
}
