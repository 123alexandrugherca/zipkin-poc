<?php

namespace App\Http\Controllers;

use App\Services\ZipkinService;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use Zipkin\Propagation\ServerHeaders;
use Zipkin\Propagation\TraceContext;
use Zipkin\Span;
use Zipkin\Timestamp;
use Zipkin\Kind;

class Controller extends BaseController
{
    public $zipkinService;

    public function __construct(ZipkinService $zipkinService)
    {
        $this->zipkinService = $zipkinService;
    }

    public function index(Request $request){

        $span = $this->zipkinService->getTracer()->nextSpan($this->zipkinService->getRootSpanContext());
        $span->annotate("Start", Timestamp\now());
        $span->setName("index_2");
        $span->start(Timestamp\now());

        $doSomething = true;

        $span->annotate("End", Timestamp\now());
        $span->finish(Timestamp\now());


        $tracer = $this->zipkinService->tracing->getTracer();
        $extractor = $this->zipkinService->tracing->getPropagation()->getExtractor(new ServerHeaders);
        $extracted = $extractor($_SERVER);
        if($extracted instanceof TraceContext) {
            // new span
            /** @var Span $span */
            $span = $tracer->newChild($extracted);
            $span->setKind(Kind\SERVER);
            $span->annotate("Start", Timestamp\now());
            $span->setName("doSomething");
            $span->start(Timestamp\now());
            sleep(1);
            $doSomething = true;

            $span->annotate("End", Timestamp\now());
            $span->finish(Timestamp\now());

            // new span child
            $spanChild = $tracer->nextSpan($span->getContext());
            $spanChild->setKind(Kind\SERVER);
            $spanChild->annotate("Start", Timestamp\now());
            $spanChild->setName("doSomething_2");
            $spanChild->start(Timestamp\now());
            sleep(1);
            $doSomething = true;

            $spanChild->annotate("End", Timestamp\now());
            $spanChild->finish(Timestamp\now());


        }

        return response([
            'success' => 'success Api2',
            'traceId' => $this->zipkinService->getRootSpanContext()->getTraceId(),
            'extracted' => get_class($extracted),
        ], 200);
    }
}

