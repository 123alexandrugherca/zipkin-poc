<?php

namespace App\Http\Controllers;

use App\Services\Propagation\CustomHeader;
use Bschmitt\Amqp\Facades\Amqp;
use Zipkin\Propagation\TraceContext;
use Zipkin\Span;
use Zipkin\Kind;
use Zipkin\Timestamp;

class RabbitMQController extends Controller
{

    public function consume()
    {
        try {
            Amqp::consume('example', function ($message, $resolver) {

                $zipkinHeaders = $message->get_properties()['application_headers']->getNativeData()['zipkin'];
                if(!empty($zipkinHeaders)) {
                    $tracer = $this->zipkinService->tracing->getTracer();
                    $extractor = $this->zipkinService->tracing->getPropagation()->getExtractor(new CustomHeader);
                    $extracted = $extractor($zipkinHeaders);
                    if($extracted instanceof TraceContext) {
                        // new span
                        /** @var Span $span */
                        $span = $tracer->newChild($extracted);
                        $span->setKind(Kind\SERVER);
                        $span->annotate("Start", Timestamp\now());
                        $span->setName("doSomething");
                        $span->start(Timestamp\now());
                        sleep(1);
                        $doSomething = true;

                        $span->annotate("End", Timestamp\now());
                        $span->finish(Timestamp\now());

                        // new span child
                        $spanChild = $tracer->nextSpan($span->getContext());
                        $spanChild->setKind(Kind\SERVER);
                        $spanChild->annotate("Start", Timestamp\now());
                        $spanChild->setName("doSomething_2");
                        $spanChild->start(Timestamp\now());
                        sleep(1);
                        $doSomething = true;

                        $spanChild->annotate("End", Timestamp\now());
                        $spanChild->finish(Timestamp\now());


                    }
                }
                $resolver->acknowledge($message);
                $resolver->stopWhenProcessed();
            });

            $data = [
                'status' => 'success',
                'traceId' => $this->zipkinService->getRootSpanContext()->getTraceId(),
            ];

            return response($data, 200);
        } catch (\Exception $exception) {
            return response('error', 500);
        }
    }

}
