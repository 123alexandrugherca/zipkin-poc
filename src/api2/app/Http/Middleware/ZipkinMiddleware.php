<?php
namespace App\Http\Middleware;

use App\Services\ZipkinService;
use Closure;


class ZipkinMiddleware
{
    private $zipkinService;

    public function __construct(ZipkinService $zipkinService)
    {
        $this->zipkinService = $zipkinService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (in_array($request->method(), $this->zipkinService->getAllowedMethods())) {

            $this->zipkinService->setTracer('API2-' . $request->path(), $request->ip());

            foreach ($request->query() as $key => $value) {
                $tags["query." . $key] = $value;
            }

            $this->zipkinService->createRootSpan('API2-incoming_request_api2', ($tags ?? []))
                ->setRootSpanMethod($request->method())
                ->setRootSpanPath($request->path())
                ->setRootSpanTag('request.headers', json_encode($request->headers->all()))
                ->setRootSpanTag('request.body', json_encode($request->all()));
        }

        return $next($request);
    }

    public function terminate($request, $response)
    {

        if (!is_null($this->zipkinService->getRootSpan())) {
            $this->zipkinService->setRootSpanStatusCode($response->getStatusCode())->closeSpan();
        }

    }

}
