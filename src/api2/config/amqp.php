<?php
return [

    'use' => 'production',

    'properties' => [

        'production' => [
            'host' => 'rabbitmq',
            'port' => 5672,
            'username' => 'admin',
            'password' => 'admin',
            'vhost' => '/',
            'exchange' => 'amq.topic',
            'exchange_type' => 'topic',
            'exchange_durable' => true,
            'exchange_auto_delete' => false,
            'exchange_internal' => false,
            'exchange_nowait' => false,
            'exchange_properties' => [],

            'queue_durable' => true,
            'ssl_options' => [],
            'connect_options' => [],
            'queue_properties' => ['x-ha-policy' => ['S', 'all']],
            'timeout' => 10,
            'persistent' => false,

            'consumer_tag'          => '',
            'consumer_no_local'     => false,
            'consumer_no_ack'       => false,
            'consumer_exclusive'    => false,
            'consumer_nowait'       => false,
        ],

    ],

];
