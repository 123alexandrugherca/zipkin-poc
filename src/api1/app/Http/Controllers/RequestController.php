<?php

namespace App\Http\Controllers;

use Bschmitt\Amqp\Facades\Amqp;
use Bschmitt\Amqp\Message;
use Bschmitt\Amqp\Table;
use GuzzleHttp\Client;
use PhpAmqpLib\Message\AMQPMessage;
use \Zipkin\Timestamp;
use Zipkin\Kind;

class RequestController extends Controller
{

    public function addToQueue()
    {
        $headerProperties = new Table([
            'zipkin' => $this->getB3PropagationHeaders()
        ]);

        $message = new Message("message".mt_rand(), [
            'content_type' => 'text/plain',
            'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT,
            'application_headers'=> $headerProperties
        ]);

        // Publish to queue
        Amqp::publish('example', $message, [
            'queue' => 'example',
        ]);

        return response([
            'success' => 'success',
            'traceId' => $this->zipkinService->getRootSpanContext()->getTraceId(),
        ], 200);
    }

    public function requestToApi2()
    {
        $span = $this->zipkinService->getTracer()->nextSpan($this->zipkinService->getRootSpanContext());
        $span->annotate("Start", Timestamp\now());
        $span->setName("request_to_api2");
        $span->start(Timestamp\now());

        // Make Request
        $endpoint = 'http://api2:8300/info';
        $method = 'GET';
        $span->setKind(Kind\CLIENT);
        $span->tag("endpoint", $endpoint);
        $span->tag("method", $method);


        $client = new Client();


        $clientRequest = $client->request($method, $endpoint, [
            'headers' => $this->getB3PropagationHeaders()
        ]);

        $span->annotate("End", Timestamp\now());
        $span->finish(Timestamp\now());

        return response([
            'success' => 'success',
            'requestBody' => json_decode($clientRequest->getBody()->getContents(), true),
            'requestStatusCode' => $clientRequest->getStatusCode(),
            'traceId' => $this->zipkinService->getRootSpanContext()->getTraceId(),
        ], 200);
    }

    /**
     * @param bool $short
     * @return array
     */
    private function getB3PropagationHeaders($short = true)
    {
        if ($short) {
            $b3 = implode('-', [
                $this->zipkinService->getRootSpanContext()->getTraceId(),
                $this->zipkinService->getRootSpanContext()->getSpanId(),
                $this->zipkinService->getRootSpanContext()->isSampled(), // optional
                $this->zipkinService->getRootSpanContext()->getSpanId(), // optional
            ]);
            return [
                'b3' => $b3
            ];

        } else {
            return [
                'X-B3-TraceId' => $this->zipkinService->getRootSpanContext()->getTraceId(),
                'X-B3-SpanId' => $this->zipkinService->getRootSpanContext()->getSpanId(),
                'X-B3-ParentId' => $this->zipkinService->getRootSpanContext()->getSpanId(),
                'X-B3-Sampled' => $this->zipkinService->getRootSpanContext()->isSampled(),
            ];
        }
    }
}
