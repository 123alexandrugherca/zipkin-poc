<?php

namespace App\Http\Controllers;

use App\Services\ZipkinService;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public $zipkinService;

    public function __construct(ZipkinService $zipkinService)
    {
        $this->zipkinService = $zipkinService;
    }

}
