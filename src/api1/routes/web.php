<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {

    return 'API1 ' . $router->app->version();
});

$router->get('/addToQueue', 'RequestController@addToQueue');

$router->get('/requestToApi2', 'RequestController@requestToApi2');
